# How to implement the Flashtalking Tag for Conversions. 

Place the tag on the confirmation step of the funnel. 
Make sure the information are listed within the parameters of the tag as such: 

We need to have new variables:
- U2: funnel_step
- U3: user_newly_registered
- U4: signup_commitment_length
- U5: signup_plan
- U6: user_uuid

## Example of the tag. 

```html
<img style="width:1px; height:1px;" 
     src="https://servedby.flashtalking.com/spot/1/{{advertiserID}};{{spotID};8207/
        ?spotName=Confirmation_Page_Corporate   
        ftXCurrency=[%INSERT_TRANSACTION_CURRENCY_HERE%]
        ftXName=[%INSERT_TRANSACTION_NAME_HERE%]        
        ftXNumItems=[%INSERT_TRANSACTION_QUANTITY_HERE%]
        ftXRef=[%INSERT_TRANSACTION_ID_HERE%]
        ftXType=[%INSERT_TRANSACTION_TYPE_HERE%]
        ftXValue=[%INSERT_TRANSACTION_VALUE_HERE%]
        U1=[%INSERT_PRODUCT_TYPE_HERE%]

        /** new Variables to add */

        U2=[%INSERT_FUNNEL_STEP_HERE%]
        U3=digital_data.user_newly_registered        
        U4=digital_data.signup_commitment_length
        U5=digital_data.signup_plan
        U6=digital_data.user_uuid
        cachebuster=[%RANDOM_NUMBER%]
"/>
```
# Flashtalking ID Table

| advertiser name | pixel name | ID string |
|------|----|------|
| BEFR | conversion | `10801;79589;8212` |
| BENL | conversion | `10800;79587;8210` |
| NLNL | conversion | `10798;79574;8207` |
