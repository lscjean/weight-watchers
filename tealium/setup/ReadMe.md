# Tealium Implementation for Weight Watchers BENELUX. 

Weight Watcher is currently using Tealium UDH (audience stream). 
Audience stream is a CDP aimed at creating visitor audiences and allow CRM enrichment with behavioral data. 
The goal of this document is to organise and give clear guidelines about the tealium implementation. 

## Scope of work

There are 2 websites : 
- Belgium: https://weightwatchers.com/be/(nl|fr)
- Netherlands: https://weightwatchers.com/nl/

Each website is divided in two sections:
- Portal (or visitor platform): contains the CMS with homepage, content and products. 
- Funnel (or signups): containing 4 steps loading with AJAX

Technical information: 
- Tealium is fired via GTM. 
- WW devs are doing as less tags as possible within their container (Tealium tag will be only 1 tag)

Tealium Implementation: 
- Define a solid data layer structure for data collection
- fire tealium tags correctly using `utag.js`, `utag.view()` & `utag.links()`
- Build use cases with Tealium. 
- Investigate CRM integration. 
- Build connection with TTD, Facebook and any other marketing channels.

## DataLayer Structure. 

all relevant information about products and pages are listed in a JS object `digital_data`. 
This object is accessible on everypage of the website. The object is populated depending on the page content. 
The most evolved version of the `digital_data` is at the thank you page

```javacript 
digital_data = {
    site_language : digital_data.site_language,
    site_region : digital_data.site_region,
    site_section : digital_data.site_section,
    site_platform : digital_data.site_platform,
    signup_channel_id: digital_data.signup_channel_id,
    signup_is_activation_path: digital_data.signup_is_activation_path,
    signup_product_type_id: digital_data.signup_product_type_id,
    signup_program: digital_data.signup_program,
    signup_program_id: digital_data.signup_program_id,
    signup_promotion_id: digital_data.signup_promotion_id,
    signup_sponsor_id: digital_data.signup_sponsor_id,
    signup_type: digital_data.signup_type,
    signup_version: digital_data.signup_version,
    signup_commitment_length: digital_data.signup_commitment_length,
    signup_is_activation_path: digital_data.signup_is_activation_path,
    signup_is_switchable: digital_data.signup_is_switchable,
    signup_member_count_cycle: digital_data.signup_member_count_cycle,
    signup_plan_total: digital_data.signup_plan_total,
    signup_subscription_length: digital_data.signup_subscription_length,
    user_newly_registered: digital_data.user_newly_registered,
    user_optin: digital_data.user_optin,
    user_unsubscribe_url: digital_data.user_unsubscribe_url,
    user_uuid: digital_data.user_uuid,
    email:digital_data.email,
}
```
Most of the data has to be captured from this object
the information should be injected in `utag_data`, aka tealium dataLayer. 

**Recommended : capturing digital_data on ww website can be also done using JS variable within Tealium IQ**

## Tealium Tags implementations. 
for this section we distinguish two main tag to fire: 
- On Page load (gtm.js) : fire the `utag.js` code. 
- On any view_changed events : fire the `utag.view()` code.
- (Optional) On any Click or link : fire the `utag.link()` code.

**Note: to fire the `utag.view`, you first need to fire `utag.js`in order to instancialize `utag`**

### Page Views
for this type of triggers, please fire the primary tealium code
the pageview tag `utag.js` will initialize tealium code and instancialize utag functions. 
this is the most important tag to place. 

To capture data at this stage, tealium require the `utag_data` object to be placed above itself. Thsi object contains the `digital_data`

### History Changes
By default, tealium does not track such event. AJAX pages requires `utag.view()` function to track correctly these hits.
Please refer to the Tealium wiki for more information about this event. 

From a view to another, some new data may exist. by the same principe as the `utag_data` object you can include information within this event as such: 
```javascript
utag.view({
    "tealium_event" : "view_changed",
    "user_optin": digital_data.user_optin,
    "user_unsubscribe_url": digital_data.user_unsubscribe_url,
    "user_uuid": digital_data.user_uuid,
    "email":digital_data.email,
})
```

### Funnel Tracking Guidelines

WW funnel is split in 4 different steps:
1. Plan: selecting the plan

when the page loads we have the following status. 
```json
digital_data = {
    page_category: "signup"
    page_name: "sign:nm:plan"
    page_type: "regular"
    pricing_engine_version: "coupon_gen"
    signup_channel_id: "8"
    signup_is_activation_path: "true"
    signup_product_type_id: "2"
    signup_program: "Meetings"
    signup_promotion_id: "6275"
    signup_sponsor_id: "14"
    signup_type: "b2c"
    signup_version: "5"
    site_env: "prod"
    site_language: "dutch"
    site_region: "nm"
    site_section: "signup"
    site_version: "2018"
}
```
2. Registration: referencing the user information

when the page loads we have the following status. 
```json
digital_data = {
    page_category: "signup",
    page_name: "sign:nm:registration"
    page_type: "regular"
    pricing_engine_version: "coupon_gen"
    signup_channel_id: "8"
    signup_commitment_length: ""
    signup_is_activation_path: "true"
    signup_is_switchable: "true"
    signup_member_count_cycle: "1"
    signup_plan: "6_maanden_gratis"
    signup_plan_total: "0.00"
    signup_product_type_id: "2"
    signup_program: "Meetings"
    signup_program_id: "54131"
    signup_promotion_id: "6275"
    signup_sponsor_id: "14"
    signup_subscription_length: 6
    signup_type: "b2c"
    signup_version: "5"
    site_env: "prod"
    site_language: "dutch"
    site_region: "nm"
    site_section: "signup"
    site_version: "2018"
}
```
3. Payment: referencing billing addresses

when the page loads we have the following status. 
```json
digital_data = {
    email: "test@test.com"
    page_category: "signup"
    page_name: "sign:nm:payment"
    page_type: "regular"
    pricing_engine_version: "coupon_gen"
    signup_channel_id: "8"
    signup_commitment_length: ""
    signup_is_activation_path: "true"
    signup_is_switchable: "true"
    signup_member_count_cycle: "1"
    signup_plan: "6_maanden_gratis"
    signup_plan_total: "0.00"
    signup_product_type_id: "2"
    signup_program: "Meetings"
    signup_program_id: "54131"
    signup_promotion_id: "6275"
    signup_sponsor_id: "14"
    signup_subscription_length: 6
    signup_type: "b2c"
    signup_version: "5"
    site_env: "prod"
    site_language: "dutch"
    site_region: "nm"
    site_section: "signup"
    site_version: "2018"
    user_newly_registered: "true"
    user_optin: "false"
    user_unsubscribe_url: "https://www.weightwatchers.com/be/nl/preferences/update?email=4989190c-8677-4c32-89e5-ace61e6f376c"
    user_uuid: "2c40d0cf-1b38-4f05-ade4-9ce844e96dd3"
}
```

4. Review: review purchase & optin
when the page loads we have the following status. 
```json
digital_data = {
    email: "test@test.com"
    page_category: "signup"
    page_name: "sign:nm:review"
    page_type: "regular"
    pricing_engine_version: "coupon_gen"
    signup_channel_id: "8"
    signup_commitment_length: ""
    signup_is_activation_path: "true"
    signup_is_switchable: "true"
    signup_member_count_cycle: "1"
    signup_plan: "6_maanden_gratis"
    signup_plan_total: "0.00"
    signup_product_type_id: "2"
    signup_program: "Meetings"
    signup_program_id: "54131"
    signup_promotion_id: "6275"
    signup_sponsor_id: "14"
    signup_subscription_length: 6
    signup_type: "b2c"
    signup_version: "5"
    site_env: "prod"
    site_language: "dutch"
    site_region: "nm"
    site_section: "signup"
    site_version: "2018"
    user_newly_registered: "true"
    user_optin: "false"
    user_unsubscribe_url: "https://www.weightwatchers.com/be/nl/preferences/update?email=4989190c-8677-4c32-89e5-ace61e6f376c"
    user_uuid: "2c40d0cf-1b38-4f05-ade4-9ce844e96dd3"
}
```

5. Confirmation: thank you page. 
when the page loads we have the following status. 
```json
digital_data = {
    email: "test@test.com"
    page_category: "signup"
    page_name: "sign:nm:confirmation"
    page_type: "regular"
    pricing_engine_version: "coupon_gen"
    signup_amount: "0"
    signup_channel_id: "8"
    signup_commitment_length: ""
    signup_is_activation_path: "true"
    signup_is_switchable: "true"
    signup_member_count_cycle: "1"
    signup_payment_method: "FLEX"
    signup_plan: "6_maanden_gratis"
    signup_plan_total: "0.00"
    signup_product_type_id: "2"
    signup_program: "Meetings"
    signup_program_id: "54131"
    signup_promotion_id: "6275"
    signup_sponsor_id: "14"
    signup_subscription_length: 6
    signup_type: "b2c"
    signup_unique_id: "69600714"
    signup_version: "5"
    site_env: "prod"
    site_language: "dutch"
    site_region: "nm"
    site_section: "signup"
    site_version: "2018"
    user_newly_registered: "true"
    user_optin: "false"
    user_unsubscribe_url: "https://www.weightwatchers.com/be/nl/preferences/update?email=4989190c-8677-4c32-89e5-ace61e6f376c"
    user_uuid: "2c40d0cf-1b38-4f05-ade4-9ce844e96dd3"
}



